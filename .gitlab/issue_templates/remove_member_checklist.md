## Operator info

- Username:  (In HSGR services)  
- Public Name:  (website)  
- HSGR mail alias: whateveryoulike@hackerspace.gr  
- Matrix user:  
- GitLab User:  

## Physical access team

- [ ] Recover Digital door key
- [ ] Unegister Digital door key

## Financial team
- [ ] Inform the [HSGR Financial team](https://www.hackerspace.gr/wiki/Financial-team) about the membership cancellation.

## Ops team:

- [ ] Remove @hackerspace.gr email alias
- [ ] Remove from HGSR Members Matrix/Element room
- [ ] Remove from developer in the [HSGR-Assembly](https://gitlab.com/hsgr/members/assembly/-/boards)
- [ ] Remove from Members list on the HSGR website
- [ ] Remove wiki user category [[Category:Members]]
- [ ] Remove from Members mailing list 
- [ ] Remove from HSGR LDAP

/confidential
/label ~members 